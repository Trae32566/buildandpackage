#!/usr/bin/env python3
#@install.py
#
# Copyright 2011 Trae Santiago
# ================================================
#
# This file is part of BuildAndPackage
#
# BuildAndPackage is free software: you 
# can redistribute it and/or modify it under the 
# terms of the GNU General Public License as 
# published by the Free Software Foundation, 
# either version 3 of the License, or (at your 
# option) any later version.
# 
# BuildAndPackage is distributed in the
# hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR 
# PURPOSE. See the GNU General Public License 
# for more details.
# 
# You should have received a copy of the GNU 
# General Public License along with BuildAndPackage. 
# If not, see: <http://www.gnu.org/licenses/>.
# (T.S / T.B / Trae32566 / Trae Santiago)

cli_print = lambda str: print(str, end = '')

def ramdisks():
    from os.path import isfile
    initRAMDisk = input('Location of inital RAM Disk: ')
    recoRAMDisk = input('Location of recovery RAM Disk: ')

    if isfile(initRAMDisk) and isfile(recoRAMDisk):
        return initRAMDisk, recoRAMDisk
    else: 
        print('Invalid path!')
        ramdisks()

#Class for revisioning systems
class RevisionSystem:
    from os import curdir, devnull, remove, sep
    from subprocess import Popen, STDOUT
    
    run = lambda command: Popen(command, stderr = STDOUT, stdout = open(devnull, 'w+b')).wait()

    def __init__(self):
        #For now we assume it's Git
        self.git()
    
    def git(self):
        #Add the remote repostory
        cli_print('o Adding BuildAndPackage repository...')
        run(['git', 'remote', 'add', 'BuildAndPackage', 'git@bitbucket.org:Trae32566/buildandpackage.git'])
        
        #Add the updater
        cli_print('done\no Adding update helper...')
        run(['git', 'configure', '--local', '--add', 'alias.updatebap', "'!git fetch BuildAndPackage -v && git merge BuildAndPackage/Release'"])
        
        #Remove their README and merge in our new changes
        cli_print('done\no Removing old README and merging...')
        self.remove('{0}{1}README'.format(curdir, sep))
        run('git', 'updatebap')
        
        #Add RAMDisks
        cli_print('done\no Adding RAM disks...')
        initRAMDisk, recoRAMDisk = ramdisks()
        run('git', 'add', '-f', initRAMDisk, recoRAMDisk)
        
        #

    def mercurial(self):
        pass

def main():
    print('NOTE: You will be prompted before any potentially hazardous activities occur.')

main()