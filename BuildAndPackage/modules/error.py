#@error.py
#
# Copyright 2011 Trae Santiago
# ================================================
#
# This file is part of BuildAndPackage
#
# BuildAndPackage is free software: you 
# can redistribute it and/or modify it under the 
# terms of the GNU General Public License as 
# published by the Free Software Foundation, 
# either version 3 of the License, or (at your 
# option) any later version.
# 
# BuildAndPackage is distributed in the
# hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR 
# PURPOSE. See the GNU General Public License 
# for more details.
# 
# You should have received a copy of the GNU 
# General Public License along with BuildAndPackage. 
# If not, see: <http://www.gnu.org/licenses/>.
# (T.S / T.B / Trae32566 / Trae Santiago

#Build errors
class BuildError:
    def __init__(self):
        print('\nBuild failed, please check log!')
        exit(1)

#Dependencies not met
class DependencyError:
    def __init__(self, dependency, required = True):
        if required:
            print('{0} is needed for proper execution of BuildAndPackage. Please install {0} and try again!'.format(dependency))
            exit(2)
        else:
            ans = input('{0} is recommended but not required for the proper execution of BuildAndPackage. Would you still like to continue installation [YES/no]? '.format(dependency)).upper()

            if ans == 'N' or 'NO': exit(0)
            else: pass

#File access issues
class FileAccessError:
    def __init__(self, fileName):
        print('\nLocked or non-existant file: ' + fileName)
        exit(3)

#Force-run caused a call to a missing dependency
class MissingDependencyError:
    def __init__(self, dependency, defunct):
        print('\nMissing dependency {0} prevents {1} from operating!'.format(dependency, defucnt))
        exit(4)

#Result out of bounds
class OutOfBoundsError:
    def __init__(self, varData, varName):
        print('\nVariable {0} out of bounds ({1})'.format(varName, varData))
        exit(5)

#Python version error
class PythonError:
    def __init__(self):
            print('Unsupported version of Python! Python 3.2 or newer is required!')
            exit(6)

#SuperUser Error
class SuperUserError:
    def __init__(self):
        print('\nPlease re-invoke this program as a standard user.')
        exit(7)

#Incorrect user settings
class UserSettingsError:
    def __init__(self, traceback):
        print('\nYour userSettings.xml file is improperly configured, please check {0}.'.format(traceback))
        exit(8)